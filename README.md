# creeper-area

#### 介绍
获取中国四级行政区数据 来源 国家统计局

住区地址名称,地址编号,数据树状存储,支持省

#### 软件架构
**依赖**
```
fastjson
lombok
jsoup
junit
```

```
├── AreaCreeper.java  ## 爬虫代码
├── AreaCreeperUtil.java ## 函数入口
├── AreaEntity.java ## 映射类(自定义修改)
└── NetWorkUrlContant.java # 常量类

```
#### 使用说明
```
抓取数据,数据存储到指定文件 AreaCreeperUtil.swoop() 
从文件解析地址数组对象     AreaCreeperUtil.parse()
```
