package com.lh.creeper.area;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;

import java.io.*;
import java.util.List;

public class AreaCreeperUtil {


    /**
     * 数据抓取
     *
     * @param file
     * @param level 1-省 2-市 3-区 4-镇
     */
    public static void swoop(File file, int level) {
        if (!file.isFile()) {
            System.out.println("错误的文件对象");
            return;
        }

        AreaCreeper areaCreeper = new AreaCreeper();
        List<AreaEntity> city = areaCreeper.execute(level);


        try {
            FileWriter fw = new FileWriter(file, false);
            JSON.writeJSONString(fw, city);
            fw.flush();
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("解析完毕");
    }


    /**
     * 数据解析
     */
    public static List<AreaEntity> parse(File file) {
        // 文件读取为JSON
        StringBuffer sb = null;

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            sb = new StringBuffer();
            String st;
            // 开始接受数据，先接受到st中
            while ((st = br.readLine()) != null) {
                // 再拼接到sd中
                sb.append(st);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return JSONArray.parseArray(sb.toString(), AreaEntity.class);
    }


}
