package com.lh.creeper.area;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

public class AreaCreeper {

    /**
     * 获取页面元素
     *
     * @param suffix 请求地址后缀
     * @param select 选择器
     * @return
     */
    Elements getElement(String suffix, String select) {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(NetWorkUrlContant.REQUEST + suffix);
        CloseableHttpResponse response = null;
        try {
            response = httpClient.execute(httpGet);
        } catch (IOException e) {
            e.printStackTrace();
        }
        HttpEntity entity = response.getEntity();
        StringBuffer sb = new StringBuffer("");
        try {
            // TODO lvheng 有空把流学透彻
            InputStream in = entity.getContent();
            BufferedReader br = new BufferedReader(new InputStreamReader(in, Charset.forName("GBK")));
            String s = "";
            while ((s = br.readLine()) != null) {
                sb.append(s);
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Document doc = Jsoup.parse(sb.toString());
        return doc.select(select);
    }


    /**
     * 解析省
     *
     * @return
     */
    List<AreaEntity> getProvince() {
        Elements elements = getElement("index.html", ".provincetr");
        List<AreaEntity> areas = new ArrayList<>();
        for (Element element : elements) {
            Elements as = element.select("a");
            for (Element a : as) {
                String href = a.attr("href");
                String name = a.childNode(0).toString();
                AreaEntity area = AreaEntity.builder().href(href).no(href.replace(".html", "")).name(name).build();
                areas.add(area);
            }
        }
        return areas;
    }


    /**
     * 1-省 2-市 3-区 4-镇
     *
     * @param level
     * @return
     */
    List<AreaEntity> execute(int level) {
        List<AreaEntity> provs;

        if (level == 0) {
            return null;
        } else {
            provs = getProvince();
        }

        if (level == 1) {
            return provs;
        }

        if (level >= 2) {
            getCity(level, provs);
        }
        return provs;
    }


    /**
     * 解析市
     *
     * @param level
     * @param provs
     */
    void getCity(int level, List<AreaEntity> provs) {
        for (AreaEntity prov : provs) {
            System.out.print("当前处理 : ");
            System.out.print(prov.getName());
            Elements elements = getElement(prov.getHref(), ".citytr");
            List<AreaEntity> areas = new ArrayList<>();
            for (Element element : elements) {
                Elements td = element.select("td > a");
                String href = td.attr("href");
                String no = td.get(0).childNode(0).toString();
                String name = td.get(1).childNode(0).toString();
                AreaEntity area = AreaEntity.builder().href(href).no(no).name(name).build();
                areas.add(area);
            }

            if (level >= 3) {
                getCounty(areas, 4 >= level);
            }

            for (int i = 0; i < String.valueOf(prov.getName()).length() + 7; i++) {
                System.out.print("\b");
            }

            System.out.println(prov.getName() + "√");
            prov.setChilds(areas);
        }
    }


    /**
     * 解析区
     *
     * @param citys
     */
    void getCounty(List<AreaEntity> citys, boolean deep) {
        for (AreaEntity city : citys) {
            System.out.print("-" + city.getName());
            Elements elements = getElement(city.getHref(), ".countytr");
            List<AreaEntity> areas = new ArrayList<>();
            if (elements.select("td").size() != 0) {
                String no1 = elements.select("td").get(0).childNode(0).toString();
                String name1 = elements.select("td").get(1).childNode(0).toString();
                AreaEntity area1 = AreaEntity.builder().no(no1).name(name1).build();
                areas.add(area1);
            }
            for (Element element : elements) {
                //遍历所有td
                Elements td = element.select("td > a");
                if (td.size() == 0) {
                    continue;
                }
                String href = td.attr("href");
                String no = td.get(0).childNode(0).toString();
                String name = td.get(1).childNode(0).toString();
                AreaEntity area = AreaEntity.builder().href(href).no(no).name(name).build();
                areas.add(area);
            }

            // 镇
            if (deep) {
                getTown(areas, city.getHref().split("/")[0]);
            }
            city.setChilds(areas);

            for (int i = 0; i < String.valueOf(city.getName()).length() + 1; i++) {
                System.out.print("\b");
            }
        }
    }


    /**
     * 解析镇
     *
     * @param counties
     */
    void getTown(List<AreaEntity> counties, String cityHref) {
        for (AreaEntity county : counties) {
            if (null == county.getHref()) {
                continue;
            }
            System.out.print("-" + county.getName());
            Elements elements = getElement(cityHref.replace(".html", "") + "/" + county.getHref(), ".towntr");
            List<AreaEntity> areas = new ArrayList<>();
            for (Element element : elements) {
                //遍历所有td
                Elements td = element.select("td > a");
                if (td.size() == 0) {
                    continue;
                }
                String href = td.attr("href");
                String no = td.get(0).childNode(0).toString();
                String name = td.get(1).childNode(0).toString();
                System.out.print("-" + name);
                AreaEntity area = AreaEntity.builder().href(href).no(no).name(name).build();
                areas.add(area);
                for (int i = 0; i < String.valueOf(name).length() + 1; i++) {
                    System.out.print("\b");
                }
            }
            county.setChilds(areas);

            for (int i = 0; i < String.valueOf(county.getName()).length() + 1; i++) {
                System.out.print("\b");
            }
        }
    }

}
