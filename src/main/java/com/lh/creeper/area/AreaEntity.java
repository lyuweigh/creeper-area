package com.lh.creeper.area;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * 地址对象
 *
 * @author lvheng
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AreaEntity implements Serializable {

    private String href;

    /**
     * 地址编号
     */
    private String no;

    /**
     * 地址名称
     */
    private String name;

    /**
     * 下级地址列表
     */
    private List<AreaEntity> childs;


}
